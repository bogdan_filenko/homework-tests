using Xunit;
using System;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using Homework.Common.DTOs.User;

#nullable enable

namespace Homework.WebAPI.IntegrationTests
{
    public class UsersControllerTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        public UsersControllerTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Theory]
        [InlineData("John", "Smith", "jSmith@gmail.com", "10/10/2002")]
        public async Task DeleteUser_WhenUserExists_ThenNoContent(string? firstName, string? lastName, string email, string birthDate)
        {
            var createUser = new CreateUserDTO()
            { 
                FirstName = firstName, 
                LastName = lastName, 
                Email = email, 
                BirthDate = DateTime.Parse(birthDate)
            };
            var content = new StringContent(JsonConvert.SerializeObject(createUser), Encoding.UTF8, "application/json");
            var createdUserJson = await (await _client.PostAsync("api/Users", content)).Content.ReadAsStringAsync();
            
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(createdUserJson);

            var httpResponse = await _client.DeleteAsync($"api/Users/{createdUser.Id}");

            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);
        }

        [Fact]
        public async Task DeleteUser_WhenUserDoesNotExist_ThenBadRequest()
        {
            var httpResponse = await _client.DeleteAsync("api/Users/1000");

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }
    }
}