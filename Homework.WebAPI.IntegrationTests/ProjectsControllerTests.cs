using Xunit;
using System;
using System.Net;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Homework.Common.DTOs.Team;
using Homework.Common.DTOs.User;
using Homework.Common.DTOs.Project;

namespace Homework.WebAPI.IntegrationTests
{
    public class ProjectsControllerTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public ProjectsControllerTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task CreateProject_WhereTeamDoesNotExist_ThenNotFound()
        {
            var createUser = new CreateUserDTO()
            { 
                FirstName = "John", 
                LastName = "Smith", 
                Email = "jSmith@gmail.com", 
                BirthDate = DateTime.Parse("10/10/1995") 
            };

            var httpResponse = await _client.PostAsync("api/Users", new StringContent(JsonConvert.SerializeObject(createUser), Encoding.UTF8, "application/json"));
            var user = JsonConvert.DeserializeObject<UserDTO>(await httpResponse.Content.ReadAsStringAsync());

            var createProject = new CreateProjectDTO()
            { 
                Name = "Rolecoaster", 
                Deadline = DateTime.Now.AddMonths(5), 
                AuthorId = user.Id 
            };
            
            httpResponse = await _client.PostAsync("api/Projects", new StringContent(JsonConvert.SerializeObject(createProject), Encoding.UTF8, "application/json"));
            var project = JsonConvert.DeserializeObject<ProjectDTO>(await httpResponse.Content.ReadAsStringAsync());
            
            await _client.DeleteAsync($"api/Users/{user.Id}");
            await _client.DeleteAsync($"api/Projects/{project.Id}");
            
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
            Assert.NotNull(project);
        }

        [Fact]
        public async Task CreateProject_WhereUserDoesNotExist_ThenNotFound()
        {
            var createTeam = new CreateTeamDTO() { Name = "WWW" };            
            
            var httpResponse = await _client.PostAsync("api/Teams", new StringContent(JsonConvert.SerializeObject(createTeam), Encoding.UTF8, "application/json"));
            var team = JsonConvert.DeserializeObject<TeamDTO>(await httpResponse.Content.ReadAsStringAsync());

            var createProject = new CreateProjectDTO() { Name = "Rolecoaster", Deadline = DateTime.Now.AddMonths(5), TeamId = team.Id };
            
            httpResponse = await _client.PostAsync("api/Projects", new StringContent(JsonConvert.SerializeObject(createProject), Encoding.UTF8, "application/json"));
            var project = JsonConvert.DeserializeObject<ProjectDTO>(await httpResponse.Content.ReadAsStringAsync());

            await _client.DeleteAsync($"api/Teams/{team.Id}");
            await _client.DeleteAsync($"api/Projects/{project.Id}");
            
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }

        [Fact]
        public async Task UpdateProject_WhenProjectExist_ThenUpdatedProject()
        {
            var createTeam = new CreateTeamDTO() { Name = "WWW" };
            var createUser = new CreateUserDTO()
            { 
                FirstName = "John", 
                LastName = "Smith", 
                Email = "jSmith@gmail.com", 
                BirthDate = DateTime.Parse("10/10/1995") 
            };
            
            var httpResponse = await _client.PostAsync("api/Teams", new StringContent(JsonConvert.SerializeObject(createTeam), Encoding.UTF8, "application/json"));
            var team = JsonConvert.DeserializeObject<TeamDTO>(await httpResponse.Content.ReadAsStringAsync());

            httpResponse = await _client.PostAsync("api/Users", new StringContent(JsonConvert.SerializeObject(createUser), Encoding.UTF8, "application/json"));
            var user = JsonConvert.DeserializeObject<UserDTO>(await httpResponse.Content.ReadAsStringAsync());

            var createProject = new CreateProjectDTO()
            { 
                Name = "Rolecoaster", 
                Deadline = DateTime.Now.AddMonths(5), 
                AuthorId = user.Id, 
                TeamId = team.Id 
            };

            httpResponse = await _client.PostAsync("api/Projects", new StringContent(JsonConvert.SerializeObject(createProject), Encoding.UTF8, "application/json"));
            var project = JsonConvert.DeserializeObject<ProjectDTO>(await httpResponse.Content.ReadAsStringAsync());

            var updateProject = new UpdateProjectDTO() { Id = project.Id, Deadline = DateTime.Now.AddMonths(6) };

            httpResponse = await _client.PutAsync("api/Projects", new StringContent(JsonConvert.SerializeObject(updateProject), Encoding.UTF8, "application/json"));

            await _client.DeleteAsync($"api/Projects/{project.Id}");
            await _client.DeleteAsync($"api/Users/{user.Id}");
            await _client.DeleteAsync($"api/Teams/{team.Id}");

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.NotEmpty(await httpResponse.Content.ReadAsStringAsync());
        }

        [Fact]
        public async Task UpdateProject_WhenProjectDoesNotExist_ThenNotFound()
        {
            var updateProject = new UpdateProjectDTO() { Id = 1000, Name = "WWW", Deadline = DateTime.Now.AddMonths(5) };

            var httpResponse = await _client.PutAsync("api/Projects", new StringContent(JsonConvert.SerializeObject(updateProject), Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }
    }
}