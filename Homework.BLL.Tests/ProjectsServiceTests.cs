using Xunit;
using System;
using AutoMapper;
using Homework.DAL.Context;
using Homework.BLL.Services;
using Homework.BLL.Exceptions;
using Homework.Common.DTOs.Project;
using Homework.BLL.MappingProfiles;
using Microsoft.EntityFrameworkCore;

namespace Homework.BLL.Tests
{
    public class ProjectsServiceTests : IDisposable
    {
        private readonly ProjectsService _projectsService;
        private readonly ProjectsContext _db;
        public ProjectsServiceTests()
        {
            var dbOptions = new DbContextOptionsBuilder<ProjectsContext>().UseInMemoryDatabase(databaseName: "ProjectsInMemory");

            _db = new ProjectsContext(dbOptions.Options);

            IMapper mapper = new MapperConfiguration(config =>
            {
                config.AddProfile<ProjectsProfile>();
            }).CreateMapper();
            
            _projectsService = new ProjectsService(_db, mapper);
        }

        public void Dispose()
        {
            _db.Database.EnsureDeleted();
        }

        [Fact]
        public void DeleteProject_WhenProjectExists_ThenNoContent()
        {   
            var createdProject = _projectsService.CreateProject(new CreateProjectDTO
            { 
                Name = "WWW",
                Deadline = DateTime.Now.AddMonths(5)
            });

            _projectsService.DeleteProject(createdProject.Id).GetAwaiter();

            Assert.Empty(_projectsService.GetProjects().Result);
        }

        [Fact]
        public void DeleteProject_WhenProjectDoesNotExist_ThenThrowNoEntityException()
        {
            Assert.ThrowsAsync<NoEntityException>(() => _projectsService.DeleteProject(1100));
        }
    }
}