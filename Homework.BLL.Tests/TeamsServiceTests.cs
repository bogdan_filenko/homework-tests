using Xunit;
using System;
using AutoMapper;
using Homework.DAL.Context;
using Homework.DAL.Entities;
using Homework.BLL.Services;
using Homework.BLL.Exceptions;
using Homework.Common.DTOs.Team;
using Homework.BLL.MappingProfiles;
using Microsoft.EntityFrameworkCore;

namespace Homework.BLL.Tests
{
    public class TeamsServiceTests : IDisposable
    {
        private readonly TeamsService _teamsService;
        private readonly ProjectsContext _db;

        public TeamsServiceTests()
        {
            var dbOptions = new DbContextOptionsBuilder<ProjectsContext>().UseInMemoryDatabase(databaseName: "ProjectsInMemory");

            _db = new ProjectsContext(dbOptions.Options);

            IMapper mapper = new MapperConfiguration(config =>
            {
                config.AddProfile<TeamsProfile>();
            }).CreateMapper();
            
            _teamsService = new TeamsService(_db, mapper);
        }

        public void Dispose()
        {
            _db.Database.EnsureDeleted();
        }

        [Fact]
        public void UpdateTeam_WhereTeamIsCorrect_ThenUpdatedTeam()
        {
            var createdTeam = new Team() { Name = "DDD", CreatedAt = DateTime.Now };
            _db.Teams.Add(createdTeam);
            _db.SaveChanges();

            var updatedTeam = _teamsService.UpdateTeam(new UpdateTeamDTO() { Id = createdTeam.Id, Name = "TTT" }).Result;

            Assert.NotNull(updatedTeam);
            Assert.Equal(createdTeam.Id, updatedTeam.Id);
            Assert.Equal(createdTeam.Name, updatedTeam.Name);
        }

        [Fact]
        public void UpdateTeam_WhereTeamDoesNotExist_ThenThrowNoEntityException()
        {
            var updateTeam = new UpdateTeamDTO() { Id = 1000, Name = "DDD" };

            Assert.ThrowsAsync<NoEntityException>(() => _teamsService.UpdateTeam(updateTeam));
        }

        [Fact]
        public void DeleteTeam_WhereTeamExists_ThenEmpty()
        {
            var createdTeam = new Team() { Name = "DDD", CreatedAt = DateTime.Now };
            _db.Teams.Add(createdTeam);
            _db.SaveChanges();

            _teamsService.DeleteTeam(createdTeam.Id).GetAwaiter();

            Assert.Empty(_teamsService.GetTeams().Result);
        }

        [Fact]
        public void DeleteTeam_WhereTeamDoesNotExist_ThenThrowNoEntityException()
        {
            Assert.ThrowsAsync<NoEntityException>(() => _teamsService.DeleteTeam(1000));
        }
    }
}