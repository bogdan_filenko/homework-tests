using Xunit;
using System;
using AutoMapper;
using Homework.DAL.Context;
using Homework.DAL.Entities;
using Homework.BLL.Services;
using Homework.BLL.Exceptions;
using Homework.Common.DTOs.User;
using Homework.BLL.MappingProfiles;
using Microsoft.EntityFrameworkCore;

namespace Homework.BLL.Tests
{
    public class UsersServiceTests : IDisposable
    {
        private readonly UsersService _usersService;
        private readonly ProjectsContext _db;
        public UsersServiceTests()
        {
            var dbOptions = new DbContextOptionsBuilder<ProjectsContext>().UseInMemoryDatabase(databaseName: "ProjectsInMemory");

            _db = new ProjectsContext(dbOptions.Options);

            IMapper mapper = new MapperConfiguration(config =>
            {
                config.AddProfile<UsersProfile>();
            }).CreateMapper();

            _usersService = new UsersService(_db, mapper);
        }

        public void Dispose()
        {
            _db.Database.EnsureDeleted();
        }

        [Fact]
        public void CreateUser_WhenInputIsCorrect_ThenGetCreatedUser()
        {
            var createUserDto = new CreateUserDTO()
            { 
                FirstName = "John", 
                LastName = "Smith", 
                Email = "jSmith@gmail.com", 
                BirthDate = DateTime.Parse("10/10/1995") 
            };

            var resultUser = _usersService.CreateUser(createUserDto).Result;

            Assert.Single(_usersService.GetUsers().Result);
        }

        [Fact]
        public void CreateUser_WhenTeamDoesNotExist_ThenThrowNoEntityException()
        {
            var createUserDto = new CreateUserDTO()
            { 
                FirstName = "John", 
                LastName = "Smith", 
                Email = "jSmith@gmail.com", 
                BirthDate = DateTime.Parse("10/10/1995"), 
                TeamId = 40
            };

            Assert.ThrowsAsync<NoEntityException>(() => _usersService.CreateUser(createUserDto));
        }

        [Fact]
        public void CreateUser_WhenEmailExists_ThenThrowEntityExistsException()
        {
            var createUserDto = new CreateUserDTO()
            { 
                FirstName = "John", 
                LastName = "Smith", 
                Email = "jSmith@gmail.com", 
                BirthDate = DateTime.Parse("10/10/1995") 
            };
            var createdUser = _usersService.CreateUser(createUserDto).Result;
            var userWithSameEmail = new CreateUserDTO { Email = createdUser.Email, BirthDate = createdUser.BirthDate };

            Assert.ThrowsAsync<EntityExistsException>(() => _usersService.CreateUser(userWithSameEmail));
        }

        [Fact]
        public void UpdateUser_WhenUserExistsAndUpdateWithAnotherTeamId_ThenUpdatedUser()
        {
            var createUserDto = new CreateUserDTO()
            { 
                FirstName = "John", 
                LastName = "Smith", 
                Email = "jSmith@gmail.com", 
                BirthDate = DateTime.Parse("10/10/1995") 
            };
            var createdUser = _usersService.CreateUser(createUserDto).Result;
            
            var team = new Team() { Name = "WWW", CreatedAt = DateTime.Now };
            _db.Teams.Add(team);
            _db.SaveChanges();

            var userUpdate = new UpdateUserDTO()
            { 
                Id = createdUser.Id, 
                FirstName = createdUser.FirstName, 
                LastName = createdUser.LastName, 
                Email = createdUser.Email, 
                BirthDate = createdUser.BirthDate, 
                TeamId = team.Id
            };

            var updatedUser = _usersService.UpdateUser(userUpdate).Result;

            Assert.NotNull(updatedUser);
            Assert.Equal(team.Id, updatedUser.TeamId);
        }

        [Fact]
        public void UpdateUser_WhenUserDoesNotExist_ThenThrowNoEntityException()
        {
            var updateUser = new UpdateUserDTO()
            { 
                Id = 1000, 
                FirstName = "John", 
                LastName = "Smith", 
                Email = "jSmith@gmail.com", 
                BirthDate = DateTime.Parse("10/10/1995") 
            };

            Assert.ThrowsAsync<NoEntityException>(() => _usersService.UpdateUser(updateUser));
        }
    }
}
