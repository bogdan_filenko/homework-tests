using Xunit;
using System;
using AutoMapper;
using Homework.DAL.Context;
using Homework.DAL.Entities;
using Homework.BLL.Services;
using Homework.BLL.Exceptions;
using Homework.Common.DTOs.Task;
using Homework.BLL.MappingProfiles;
using Microsoft.EntityFrameworkCore;

namespace Homework.BLL.Tests
{
    public class TasksServiceTests : IDisposable
    {
        private readonly TasksService _tasksService;
        private readonly ProjectsContext _db;
        public TasksServiceTests()
        {
            var dbOptions = new DbContextOptionsBuilder<ProjectsContext>().UseInMemoryDatabase(databaseName: "ProjectsInMemory");

            _db = new ProjectsContext(dbOptions.Options);

            IMapper mapper = new MapperConfiguration(config =>
            {
                config.AddProfile<TasksProfile>();
            }).CreateMapper();
            
            _tasksService = new TasksService(_db, mapper);
        }

        public void Dispose()
        {
            _db.Database.EnsureDeleted();
        }

        [Fact]
        public void UpdateTaks_WhenTaskStateIsFinished_ThenFinishedTask()
        {
            var user = new User()
            {
                FirstName = "John",
                LastName = "Smith",
                Email = "jSmith@gmail.com",
                RegisteredAt = DateTime.Now,
                BirthDate = DateTime.Parse("10/10/2002")
            };
            var task = new Task() { Name = "Json serialization", State = 0, CreatedAt = DateTime.Now };
            _db.Tasks.Add(task);
            _db.Users.Add(user);
            _db.SaveChanges();
            
            var updatedTask = _tasksService.UpdateTask(new UpdateTaskDTO()
            {
                Id = task.Id,
                Name = "Json serialization",
                State = 2,
                PerformerId = user.Id
            }).Result;

            Assert.NotNull(updatedTask);
            Assert.NotNull(updatedTask.FinishedAt);
        }

        [Fact]
        public void UpdateTask_WhenTaskDoesNotExist_ThenThrowNoEntityException()
        {
            var updateTask = new UpdateTaskDTO()
            {
                Id = 1000,
                Name = "Json serialization",
                State = 2
            };

            Assert.ThrowsAsync<NoEntityException>(() => _tasksService.UpdateTask(updateTask));
        }

        [Fact]
        public void CreateTask_WhenTaskIsCorrect_ThenCreatedTask()
        {
            var project = new Project() { Name = "WWW", Deadline = DateTime.Now.AddMonths(6), CreatedAt = DateTime.Now };
            var user = new User()
            {
                FirstName = "John",
                LastName = "Smith",
                Email = "jSmith@gmail.com",
                RegisteredAt = DateTime.Now,
                BirthDate = DateTime.Parse("10/10/2002")
            };
            _db.Projects.Add(project);
            _db.Users.Add(user);
            _db.SaveChanges();

            var createTask = new CreateTaskDTO()
            { 
                Name = "Json serialization",
                PerformerId = user.Id, 
                ProjectId = project.Id
            };

            var createdTask = _tasksService.CreateTask(createTask).Result;

            Assert.Single(_tasksService.GetTasks().Result);
        }

        [Fact]
        public void CreateTask_WhenPerformerDoesNotExist_ThenThrowNoEntityException()
        {
            var project = new Project()
            {
                Name = "WWW",
                Deadline = DateTime.Now.AddMonths(6),
                CreatedAt = DateTime.Now
            };
            _db.Projects.Add(project);
            _db.SaveChanges();
            
            var createTask = new CreateTaskDTO()
            {
                Name = "Json serialization",
                ProjectId = project.Id
            };

            Assert.ThrowsAsync<NoEntityException>(() => _tasksService.CreateTask(createTask));
        }

        [Fact]
        public void CreateTask_WhenProjectDoesNotExist_ThenThrowNoEntityException()
        {
            var user = new User()
            {
                FirstName = "John",
                LastName = "Smith",
                Email = "jSmith@gmail.com",
                RegisteredAt = DateTime.Now,
                BirthDate = DateTime.Parse("10/10/2002")
            };
            _db.Users.Add(user);
            _db.SaveChanges();
            
            var createTask = new CreateTaskDTO() { Name = "Json serialization", PerformerId = user.Id };

            Assert.ThrowsAsync<NoEntityException>(() => _tasksService.CreateTask(createTask));
        }
    }
}
