using System;
using System.Collections.Generic;
using Homework.Client.Models;
using Homework.Common.DTOs.Task;
using Homework.Client.DialogExecutors.Abstract;

namespace Homework.Client.DialogExecutors
{
    public sealed class TasksDialogExecutor : DialogExecutor
    {

        public TasksDialogExecutor(Cui cui) : base(cui)
        {
        }

        public override async System.Threading.Tasks.Task Execute()
        {
            while(_isOpened)
            {
                _cui.ShowTasksDialog();
                char option = Console.ReadKey(true).KeyChar;
                
                try
                {
                    switch(option)
                    {
                        case '1':
                        {
                            var tasks = await _httpService.Get<IEnumerable<TaskDTO>>("/Tasks");
                            _dataViewer.ViewCollectionData<TaskDTO>(tasks);

                            ResetScreen();
                            break;
                        }
                        case '2':
                        {
                            int? taskId = GetTaskId();
                            if (taskId.HasValue)
                            {
                                var task = await _httpService.Get<TaskDTO>($"/Tasks/{taskId.Value}");
                                _dataViewer.ViewSingleEntity<TaskDTO>(task);
                            }
                            ResetScreen();
                            break;
                        }
                        case '3':
                        {
                            var task = await _httpService.Post<CreateTaskDTO, TaskDTO>("/Tasks", ExecuteCreateTaskDialog());
                            _dataViewer.ViewSingleEntity<TaskDTO>(task);

                            ResetScreen();
                            break;
                        }
                        case '4':
                        {
                            int? taskId = GetTaskId();
                            if (taskId.HasValue)
                            {
                                var oldTask = await _httpService.Get<TaskDTO>($"/Tasks/{taskId.Value}");

                                var task = await _httpService.Put<UpdateTaskDTO, TaskDTO>("/Tasks", ExecuteUpdateTaskDialog(oldTask));
                                _dataViewer.ViewSingleEntity<TaskDTO>(task);
                            }
                            ResetScreen();
                            break;
                        }
                        case '5':
                        {
                            int? taskId = GetTaskId();
                            if (taskId.HasValue)
                            {
                                await _httpService.Delete($"/Tasks/{taskId.Value}");
                            }
                            ResetScreen();
                            break;
                        }
                        case '6':
                        {
                            _isOpened = false;
                            break;
                        }
                        default:
                        {
                            _cui.ShowMessage("Unknown option");
                            ResetScreen();
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _cui.ShowMessage(ex.Message);
                    ResetScreen();
                }
            }
        }

        private int? GetTaskId()
        {
            _cui.ShowTaskIdDialog();

            if (!int.TryParse(Console.ReadLine(), out int taskId))
            {
                _cui.ShowMessage("Invalid task`s id format");
                return default;
            }
            return taskId;
        }

        private CreateTaskDTO ExecuteCreateTaskDialog()
        {
            _cui.ShowMessage("Type task`s name");
            string name = Console.ReadLine();

            _cui.ShowMessage("Type task`s description");
            string description = Console.ReadLine();

            DateTime deadline = DateTime.Now.AddMonths(6);

            _cui.ShowMessage("Type task`s project id");
            if (!int.TryParse(Console.ReadLine(), out int projectId))
            {
                throw new Exception("Invalid format of project`s id");
            }

            _cui.ShowMessage("Type task`s performer id");
            if (!int.TryParse(Console.ReadLine(), out int performerId))
            {
                throw new Exception("Invalid format of performer`s id");
            }

            return new CreateTaskDTO
            {
                Name = name,
                Description = description,
                ProjectId = projectId,
                PerformerId = performerId
            };
        }

        private UpdateTaskDTO ExecuteUpdateTaskDialog(TaskDTO oldTask)
        {
            _cui.ShowMessage("Type task`s name");
            string name = Console.ReadLine();

            _cui.ShowMessage("Type task`s description");
            string description = Console.ReadLine();

            _cui.ShowMessage("Type task`s status code");
            if (!int.TryParse(Console.ReadLine(), out int stateCode) && stateCode >= 0 && stateCode <= 3)
            {
                throw new Exception("Invalid format of state`s code");
            }

            _cui.ShowMessage("Type task`s performer id");
            if (!int.TryParse(Console.ReadLine(), out int performerId))
            {
                throw new Exception("Invalid format of performer`s id");
            }
            
            return new UpdateTaskDTO
            {
                Id = oldTask.Id,
                Name = string.IsNullOrEmpty(name) ? oldTask.Name : name,
                Description = string.IsNullOrEmpty(description) ? oldTask.Description : description,
                State = stateCode,
                PerformerId = performerId
            };
        }
    }
}