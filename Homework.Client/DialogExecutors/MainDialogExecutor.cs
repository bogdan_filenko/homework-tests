using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Homework.Common.DTOs.Linq;
using Homework.Client.DialogExecutors.Abstract;

namespace Homework.Client.DialogExecutors
{
    public sealed class MainDialogExecutor : DialogExecutor
    {
        public MainDialogExecutor(Cui cui) : base(cui)
        {
        }

        public override async Task Execute()
        {
            while(_isOpened)
            {
                try
                {
                    _cui.ShowMainDialog();
                    char option = Console.ReadKey(true).KeyChar;

                    switch(option)
                    {
                        case '1':
                        {
                            int? userId = ExecuteUserIdDialog();
                            if (userId != default)
                            {
                                _cui.ShowMessage("Not implemented :(");
                            }
                            
                            ResetScreen();
                            break;
                        }
                        case '2':
                        {
                            int? userId = ExecuteUserIdDialog();
                            if (userId != default)
                            {
                                var response = await _httpService.Get<IEnumerable<TaskLinqDTO>>($"/Linq/users/{userId}/tasks");
                                _dataViewer.ViewCollectionData<TaskLinqDTO>(response);
                            }

                            ResetScreen();
                            break;
                        }
                        case '3':
                        {
                            int? userId = ExecuteUserIdDialog();
                            if (userId != default)
                            {
                                var response = await _httpService.Get<IEnumerable<ShortTaskDTO>>($"/Linq/users/{userId}/tasks/finished");
                                _dataViewer.ViewCollectionData<ShortTaskDTO>(response);
                            }

                            ResetScreen();
                            break;
                        }
                        case '4':
                        {
                            var response = await _httpService.Get<IEnumerable<ShortTeamDTO>>("/Linq/teams/users/ordered");
                            _dataViewer.ViewCollectionData<ShortTeamDTO>(response);
                            
                            ResetScreen();
                            break;
                        }
                        case '5':
                        {
                            var response = await _httpService.Get<IEnumerable<UserLinqDTO>>("/Linq/users/tasks/ordered");
                            _dataViewer.ViewCollectionData<UserLinqDTO>(response);

                            ResetScreen();
                            break;
                        }
                        case '6':
                        {
                            int? userId = ExecuteUserIdDialog();
                            if (userId != default)
                            {
                                var response = await _httpService.Get<UserInfoDTO>($"/Linq/users/{userId}/info");
                                _dataViewer.ViewSingleEntity<UserInfoDTO>(response);
                            }

                            ResetScreen();
                            break;
                        }
                        case '7':
                        {
                            var response = await _httpService.Get<IEnumerable<ProjectInfoDTO>>("/Linq/projects/info");
                            _dataViewer.ViewCollectionData<ProjectInfoDTO>(response);

                            ResetScreen();
                            break;
                        }
                        case '8':
                        {
                            await new OthersDialogExecutor(_cui).Execute();
                            break;
                        }
                        case '9':
                        {
                            _cui.ShowMessage("Program is closing");
                            _isOpened = false;
                            break;
                        }
                        default:
                        {
                            _cui.ShowMessage("Unknown option");
                            ResetScreen();
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _cui.ShowMessage(ex.Message);
                    ResetScreen();
                }
            }
        }

        public int? ExecuteUserIdDialog()
        {
            _cui.ShowUserIdDialog();
            if (!int.TryParse(Console.ReadLine(), out int userId))
            {
                _cui.ShowMessage("Invalid user`s id format");
                return default;
            }
            return userId;
        }
    }
}