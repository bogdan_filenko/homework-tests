using System;
using System.Collections.Generic;
using Homework.Common.DTOs.Team;
using Homework.Client.DialogExecutors.Abstract;

namespace Homework.Client.DialogExecutors
{
    public sealed class TeamsDialogExecutor : DialogExecutor
    {

        public TeamsDialogExecutor(Cui cui) : base(cui)
        {
        }

        public override async System.Threading.Tasks.Task Execute()
        {
            while(_isOpened)
            {
                _cui.ShowTeamsDialog();
                char option = Console.ReadKey(true).KeyChar;
                try
                {
                    switch(option)
                    {
                        case '1':
                        {
                            var teams = await _httpService.Get<IEnumerable<TeamDTO>>("/Teams");
                            _dataViewer.ViewCollectionData<TeamDTO>(teams);

                            ResetScreen();
                            break;
                        }
                        case '2':
                        {
                            int? teamId = GetTeamId();
                            if (teamId.HasValue)
                            {
                                var team = await _httpService.Get<TeamDTO>($"/Teams/{teamId.Value}");
                                _dataViewer.ViewSingleEntity<TeamDTO>(team);
                            }
                            ResetScreen();
                            break;
                        }
                        case '3':
                        {
                            var team = await _httpService.Post<CreateTeamDTO, TeamDTO>("/Teams", ExecuteCreateTeamDialog());
                            _dataViewer.ViewSingleEntity<TeamDTO>(team);

                            ResetScreen();
                            break;
                        }
                        case '4':
                        {
                            int? teamId = GetTeamId();
                            if (teamId.HasValue)
                            {
                                var oldTeam = await _httpService.Get<TeamDTO>($"/Teams/{teamId.Value}");

                                var team = await _httpService.Put<UpdateTeamDTO, TeamDTO>("/Teams", ExecuteUpdateTeamDialog(oldTeam));
                                _dataViewer.ViewSingleEntity<TeamDTO>(team);
                            }
                            ResetScreen();
                            break;
                        }
                        case '5':
                        {
                            int? teamId = GetTeamId();
                            if (teamId.HasValue)
                            {
                                await _httpService.Delete($"/Teams/{teamId.Value}");
                            }
                            ResetScreen();
                            break;
                        }
                        case '6':
                        {
                            _isOpened = false;
                            break;
                        }
                        default:
                        {
                            _cui.ShowMessage("Unknown option");
                            ResetScreen();
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _cui.ShowMessage(ex.Message);
                    ResetScreen();
                }
            }
        }
        
        private int? GetTeamId()
        {
            _cui.ShowTeamIdDialog();

            if (!int.TryParse(Console.ReadLine(), out int teamId))
            {
                _cui.ShowMessage("Invalid team`s id format");
                return default;
            }
            return teamId;
        }

        private CreateTeamDTO ExecuteCreateTeamDialog()
        {
            _cui.ShowMessage("Type team`s name");
            string name = Console.ReadLine();

            return new CreateTeamDTO
            {
                Name = name
            };
        }

        private UpdateTeamDTO ExecuteUpdateTeamDialog(TeamDTO oldTeam)
        {
            _cui.ShowMessage("Type team`s name");
            string name = Console.ReadLine();
            
            return new UpdateTeamDTO
            {
                Id = oldTeam.Id,
                Name = string.IsNullOrEmpty(name) ? oldTeam.Name : name
            };
        }
    }
}