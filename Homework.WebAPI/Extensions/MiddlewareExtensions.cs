using Microsoft.AspNetCore.Builder;
using Homework.WebAPI.Middlewares;

namespace Homework.WebAPI.Extensions
{
    public static class MiddlewareExtensions
    {
        public static void UseErrorsMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ErrorHandleMiddleware>();
        }
    }
}