using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Homework.BLL.Interfaces;
using Homework.Common.DTOs.Team;

namespace Homework.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamsService _teamsService;
        public TeamsController(ITeamsService teamsService)
        {
            _teamsService = teamsService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> Get()
        {
            return Ok(await _teamsService.GetTeams());
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> Get(int id)
        {
            return Ok(await _teamsService.GetTeamById(id));
        }

        [HttpPost]
        public async Task<ActionResult<TeamDTO>> Post([FromBody] CreateTeamDTO dto)
        {
            return Ok(await _teamsService.CreateTeam(dto));
        }

        [HttpPut]
        public async Task<ActionResult<TeamDTO>> Put([FromBody] UpdateTeamDTO dto)
        {
            return Ok(await _teamsService.UpdateTeam(dto));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _teamsService.DeleteTeam(id);

            return NoContent();
        }
    }
}