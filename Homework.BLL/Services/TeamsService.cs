using AutoMapper;
using System;
using System.Threading.Tasks;
using Homework.DAL.Context;
using Homework.DAL.Entities;
using System.Collections.Generic;
using Homework.BLL.Interfaces;
using Homework.BLL.Exceptions;
using Homework.Common.DTOs.Team;
using Microsoft.EntityFrameworkCore;

namespace Homework.BLL.Services
{
    public sealed class TeamsService : ITeamsService
    {
        private readonly IMapper _mapper;
        private readonly ProjectsContext _db;

        public TeamsService(ProjectsContext context, IMapper mapper)
        {
            _db = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TeamDTO>> GetTeams()
        {
            var teamsList = await _db.Teams.AsNoTracking().ToListAsync();

            return _mapper.Map<IEnumerable<TeamDTO>>(teamsList);
        }

        public async Task<TeamDTO> GetTeamById(int id)
        {
            var team = await _db.Teams.AsNoTracking().FirstOrDefaultAsync(t => t.Id == id);

            if (team == default)
            {
                throw new NoEntityException(typeof(Team), id);
            }

            return _mapper.Map<TeamDTO>(team);
        }

        public async Task<TeamDTO> CreateTeam(CreateTeamDTO createDto)
        {
            if (await _db.Teams.AsNoTracking().FirstOrDefaultAsync(t => t.Name == createDto.Name) != default)
            {
                throw new EntityExistsException(typeof(Team), "name", createDto.Name);
            }

            var newTeam = _mapper.Map<Team>(createDto);
            _db.Teams.Add(newTeam);
            await _db.SaveChangesAsync();

            return _mapper.Map<TeamDTO>(newTeam);
        }

        public async Task<TeamDTO> UpdateTeam(UpdateTeamDTO updateDto)
        {
            if (await _db.Teams.AsNoTracking().FirstOrDefaultAsync(t => t.Name == updateDto.Name) != default)
            {
                throw new EntityExistsException(typeof(Team), "name", updateDto.Name);
            }

            var updatableTeam = await _db.Teams.FirstOrDefaultAsync(t => t.Id == updateDto.Id);

            if (updatableTeam == default)
            {
                throw new NoEntityException(typeof(Team), updateDto.Id);
            }

            updatableTeam.Name = updateDto.Name;

            await _db.SaveChangesAsync();

            return _mapper.Map<TeamDTO>(updatableTeam);
        }

        public async System.Threading.Tasks.Task DeleteTeam(int id)
        {
            var removableTeam = await _db.Teams.FirstOrDefaultAsync(t => t.Id == id);

            if (removableTeam == default)
            {
                throw new NoEntityException(typeof(Team), id);
            }

            _db.Teams.Remove(removableTeam);
            await _db.SaveChangesAsync();
        }
    }
}