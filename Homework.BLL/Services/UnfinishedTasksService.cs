using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using Homework.DAL.Context;
using Homework.DAL.Entities;
using Homework.BLL.Interfaces;
using Homework.BLL.Exceptions;
using System.Collections.Generic;
using Homework.Common.DTOs.Task;
using Microsoft.EntityFrameworkCore;

namespace Homework.BLL.Services
{
    public sealed class UnfinishedTasksService : IUnfinishedTasksService
    {
        private readonly IMapper _mapper;
        private readonly ProjectsContext _db;

        public UnfinishedTasksService(ProjectsContext context, IMapper mapper)
        {
            _db = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TaskDTO>> GetUnfinishedTasks(int userId)
        {
            if (await _db.Users.FirstOrDefaultAsync(u => u.Id == userId) == default)
            {
                throw new NoEntityException(typeof(User), userId);
            }

            return _mapper.Map<IEnumerable<TaskDTO>>(_db.Tasks.Where(t => t.PerformerId == userId && t.State != 2));
        }
    }
}