using System;
using AutoMapper;
using System.Linq;
using Homework.DAL.Context;
using Homework.BLL.Interfaces;
using System.Collections.Generic;
using Homework.Common.DTOs.Linq;
using Microsoft.EntityFrameworkCore;

namespace Homework.BLL.Services
{
    public sealed class LinqService : ILinqService
    {
        private readonly ProjectsContext _db;
        private readonly IMapper _mapper;

        private const int CURRENT_YEAR = 2021;
        private const int FINISHED_TASK_CODE = 2;

        public LinqService(ProjectsContext context, IMapper mapper)
        {
            _db = context;
            _mapper = mapper;
        }
        public IDictionary<ProjectLinqDTO, int> GetProjectsTaskNumber(int userId)
        {
            var composedData = _db.Projects
                .Include(p => p.Tasks)
                .Include(p => p.Author)
                .AsNoTracking();

            var mappedData = _mapper.Map<ProjectLinqDTO[]>(composedData);

            return mappedData
                .Where(p => p.Author.Id == userId)
                .ToDictionary(
                    project => project,
                    project => project.Tasks.Length);
        }

        public IEnumerable<TaskLinqDTO> GetAllUserTasks(int userId)
        {
            var composedData = _db.Projects
                .Include(p => p.Tasks)
                    .ThenInclude(t => t.Performer)
                .AsNoTracking();

            var mappedData = _mapper.Map<ProjectLinqDTO[]>(composedData);

            return mappedData
                .SelectMany(p => p.Tasks)
                .Where(t => t.Performer.Id == userId && t.Name.Length < 45);
        }

        public IEnumerable<ShortTaskDTO> GetFinishedTasks(int userId)
        {
            var composedData = _db.Projects
                .Include(p => p.Tasks)
                    .ThenInclude(t => t.Performer)
                .AsNoTracking();

            var mappedData = _mapper.Map<ProjectLinqDTO[]>(composedData); 

            return composedData
                .SelectMany(p => p.Tasks)
                .Where(t => t.FinishedAt != default && t.FinishedAt.Value.Year == CURRENT_YEAR && t.Performer.Id == userId)
                .Select(t => _mapper.Map<ShortTaskDTO>(t));
        }
        
        public IEnumerable<ShortTeamDTO> GetTeamsWhereUsersOlderThan10()
        {
            var composedData = _db.Projects
                .Include(p => p.Team)
                    .ThenInclude(t => t.Members)
                .AsNoTracking();

            var mappedData = _mapper.Map<ProjectLinqDTO[]>(composedData);

            return mappedData
                .Select(p => p.Team)
                .Distinct()
                .Where(t => t.Members.All(u => DateTime.Now.Year - u.BirthDate.Year > 10))
                .Select(t =>
                {
                    var shortTeamDto = _mapper.Map<ShortTeamDTO>(t);
                    shortTeamDto.Members = t.Members.OrderByDescending(u => u.RegisteredAt).ToArray();

                    return shortTeamDto;
                });
        }
        
        public IEnumerable<UserLinqDTO> GetUsersWithSortedTasks()
        {
            var composedData = _db.Projects
                .Include(p => p.Team)
                    .ThenInclude(t => t.Members)
                        .ThenInclude(u => u.Tasks)
                .AsNoTracking();

            var mappedData = _mapper.Map<ProjectLinqDTO[]>(composedData);

            return mappedData
                .SelectMany(p => p.Team.Members)
                .Select(u =>
                {
                    u.Tasks = u.Tasks.OrderByDescending(t => t.Name.Length).ToArray();
                    return u;
                })
                .OrderBy(u => u.FirstName);
        }

        public UserInfoDTO GetUserInfo(int userId)
        {
            var composedData = _db.Users
                .Include(u => u.Tasks)
                .Include(u => u.Projects)
                .AsNoTracking();

            var mappedData = _mapper.Map<UserLinqDTO[]>(composedData);

            return mappedData
                .Where(u => u.Id == userId)
                .Select(u => new UserInfoDTO()
                {
                    User = u,
                    LastProject = u.Projects
                        ?.OrderByDescending(p => p.CreatedAt)
                        ?.FirstOrDefault() ?? null,
                    LastProjectTotalTasks = u.Projects
                        ?.OrderByDescending(p => p.CreatedAt)
                        ?.FirstOrDefault()?.Tasks?.Length ?? 0,
                    TotalUnfinishedTasks = u?.Tasks.Count(t => t.State != FINISHED_TASK_CODE) ?? 0,
                    LongestTask = u?.Tasks.OrderByDescending(t => t.FinishedAt - t.CreatedAt).FirstOrDefault() ?? null
                })
                .FirstOrDefault();
        }

        public IEnumerable<ProjectInfoDTO> GetProjectsInfo()
        {
            var composedData =_db.Projects
                .Include(p => p.Team)
                    .ThenInclude(t => t.Members)
                .Include(p => p.Tasks)
                .AsNoTracking();

            var mappedData = _mapper.Map<ProjectLinqDTO[]>(composedData);

            return mappedData
                .Where(p => p.Description.Length > 20 || p.Tasks.Length < 3)
                .Select(p => new ProjectInfoDTO()
                {
                    Project = p,
                    LongestByDescTask = p.Tasks
                        .OrderByDescending(t => t.Description)
                        .FirstOrDefault(),
                    ShortestByNameTask = p.Tasks
                        .OrderBy(t => t.Name)
                        .FirstOrDefault(),
                    TotalProjectParticians = p.Team.Members?.Length ?? 0
                });
        }
    }
}