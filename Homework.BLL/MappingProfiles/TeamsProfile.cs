using System;
using AutoMapper;
using Homework.DAL.Entities;
using Homework.Common.DTOs.Team;

namespace Homework.BLL.MappingProfiles
{
    public sealed class TeamsProfile : Profile
    {
        public TeamsProfile()
        {
            CreateMap<Team, TeamDTO>();

            CreateMap<CreateTeamDTO, Team>()
                .ForMember(dest => dest.CreatedAt, src => src.MapFrom(t => DateTime.Now));
        }
    }
}