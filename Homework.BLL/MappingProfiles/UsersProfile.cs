using System;
using AutoMapper;
using Homework.DAL.Entities;
using Homework.Common.DTOs.User;

namespace Homework.BLL.MappingProfiles
{
    public sealed class UsersProfile : Profile
    {
        public UsersProfile()
        {
            CreateMap<User, UserDTO>();

            CreateMap<CreateUserDTO, User>()
                .ForMember(dest => dest.RegisteredAt, src => src.MapFrom(u => DateTime.Now));
        }
    }
}