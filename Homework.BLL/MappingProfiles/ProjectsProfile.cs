using System;
using AutoMapper;
using Homework.DAL.Entities;
using Homework.Common.DTOs.Project;

namespace Homework.BLL.MappingProfiles
{
    public sealed class ProjectsProfile : Profile
    {
        public ProjectsProfile()
        {
            CreateMap<Project, ProjectDTO>();

            CreateMap<CreateProjectDTO, Project>()
                .ForMember(dest => dest.CreatedAt, src => src.MapFrom(p => DateTime.Now));
        }
    }
}