using System.Threading.Tasks;
using System.Collections.Generic;
using Homework.Common.DTOs.Team;

namespace Homework.BLL.Interfaces
{
    public interface ITeamsService
    {
        Task<IEnumerable<TeamDTO>> GetTeams();
        Task<TeamDTO> GetTeamById(int id);
        Task<TeamDTO> CreateTeam(CreateTeamDTO createDto);
        Task<TeamDTO> UpdateTeam(UpdateTeamDTO updateDto);
        Task DeleteTeam(int id);
    }
}