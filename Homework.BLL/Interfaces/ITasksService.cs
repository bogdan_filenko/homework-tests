using System.Threading.Tasks;
using System.Collections.Generic;
using Homework.Common.DTOs.Task;

namespace Homework.BLL.Interfaces
{
    public interface ITasksService
    {
        Task<IEnumerable<TaskDTO>> GetTasks();
        Task<TaskDTO> GetTaskById(int id);
        Task<TaskDTO> CreateTask(CreateTaskDTO createDto);
        Task<TaskDTO> UpdateTask(UpdateTaskDTO updateDto);
        Task DeleteTask(int id);
    }
}