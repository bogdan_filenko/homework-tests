using System.Threading.Tasks;
using System.Collections.Generic;
using Homework.Common.DTOs.User;

namespace Homework.BLL.Interfaces
{
    public interface IUsersService
    {
        Task<IEnumerable<UserDTO>> GetUsers();
        Task<UserDTO> GetUserById(int id);
        Task<UserDTO> CreateUser(CreateUserDTO createDto);
        Task<UserDTO> UpdateUser(UpdateUserDTO updateDto);
        Task DeleteUser(int id);
    }
}