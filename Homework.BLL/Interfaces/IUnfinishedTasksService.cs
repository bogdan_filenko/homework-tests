using System.Threading.Tasks;
using System.Collections.Generic;
using Homework.Common.DTOs.Task;

namespace Homework.BLL.Interfaces
{
    public interface IUnfinishedTasksService
    {
        Task<IEnumerable<TaskDTO>> GetUnfinishedTasks(int userId);
    }
}