﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Homework.DAL.Migrations
{
    public partial class RelationsChanging : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 9, new DateTime(2021, 7, 3, 23, 17, 4, 612, DateTimeKind.Local).AddTicks(3757), new DateTime(2023, 1, 9, 9, 29, 3, 634, DateTimeKind.Local).AddTicks(2495), "Technician Seychelles Rupee Small Concrete Shoes", "Chief Usability Planner", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 26, new DateTime(2021, 7, 3, 23, 17, 4, 614, DateTimeKind.Local).AddTicks(2996), new DateTime(2023, 7, 3, 4, 55, 18, 399, DateTimeKind.Local).AddTicks(1094), "Sports, Beauty & Grocery indexing", "Direct Quality Officer", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 9, new DateTime(2021, 7, 3, 23, 17, 4, 614, DateTimeKind.Local).AddTicks(4766), new DateTime(2022, 2, 26, 23, 44, 0, 177, DateTimeKind.Local).AddTicks(7956), "Light", "Lead Identity Director" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 13, new DateTime(2021, 7, 3, 23, 17, 4, 614, DateTimeKind.Local).AddTicks(5884), new DateTime(2023, 2, 8, 8, 8, 44, 116, DateTimeKind.Local).AddTicks(9790), "Incredible Dynamic Administrator", "Corporate Markets Engineer", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 16, new DateTime(2021, 7, 3, 23, 17, 4, 614, DateTimeKind.Local).AddTicks(6071), new DateTime(2021, 10, 31, 1, 54, 32, 591, DateTimeKind.Local).AddTicks(8113), "Practical", "Human Program Producer", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 4, new DateTime(2021, 7, 3, 23, 17, 4, 614, DateTimeKind.Local).AddTicks(7317), new DateTime(2022, 2, 24, 10, 14, 28, 460, DateTimeKind.Local).AddTicks(1518), "invoice", "Dynamic Research Facilitator", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 19, new DateTime(2021, 7, 3, 23, 17, 4, 614, DateTimeKind.Local).AddTicks(7661), new DateTime(2021, 12, 19, 22, 3, 22, 666, DateTimeKind.Local).AddTicks(3906), "Handmade Plastic Fish", "Principal Quality Coordinator", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 614, DateTimeKind.Local).AddTicks(7791), new DateTime(2022, 2, 11, 18, 45, 38, 146, DateTimeKind.Local).AddTicks(6438), "Manors", "Central Intranet Consultant", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 29, new DateTime(2021, 7, 3, 23, 17, 4, 614, DateTimeKind.Local).AddTicks(9461), new DateTime(2021, 10, 27, 4, 51, 38, 599, DateTimeKind.Local).AddTicks(6187), "Credit Card Account", "National Interactions Manager" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 6, new DateTime(2021, 7, 3, 23, 17, 4, 615, DateTimeKind.Local).AddTicks(2271), new DateTime(2023, 4, 10, 9, 14, 47, 311, DateTimeKind.Local).AddTicks(6935), "software Sports JBOD", "Dynamic Program Agent", 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 623, DateTimeKind.Local).AddTicks(9736), "Incredible", new DateTime(2021, 7, 3, 23, 17, 4, 624, DateTimeKind.Local).AddTicks(4187), "payment", 6, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 626, DateTimeKind.Local).AddTicks(1468), "back up Bedfordshire vortals", new DateTime(2021, 7, 3, 23, 17, 4, 626, DateTimeKind.Local).AddTicks(1504), "Vermont", 24, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 626, DateTimeKind.Local).AddTicks(2183), "withdrawal Research North Carolina", null, "Lock", 15, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 626, DateTimeKind.Local).AddTicks(2351), "payment Stravenue", "Developer", 15, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 626, DateTimeKind.Local).AddTicks(3524), "Serbia", new DateTime(2021, 7, 3, 23, 17, 4, 626, DateTimeKind.Local).AddTicks(3552), "Avon", 13, 9, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 626, DateTimeKind.Local).AddTicks(4878), "New Mexico Seamless", "portals", 13, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 626, DateTimeKind.Local).AddTicks(5528), "Cotton productivity", "Wells", 17, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 626, DateTimeKind.Local).AddTicks(6879), "disintermediate Frozen Guarani", "transmitting", 18, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 626, DateTimeKind.Local).AddTicks(8196), "Ergonomic", "program", 11, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 626, DateTimeKind.Local).AddTicks(9655), "architectures web services seamless", new DateTime(2021, 7, 3, 23, 17, 4, 626, DateTimeKind.Local).AddTicks(9685), "SSL", 14, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 626, DateTimeKind.Local).AddTicks(9846), "New Jersey Officer", null, "withdrawal", 3, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 626, DateTimeKind.Local).AddTicks(9962), "Home", "Convertible Marks", 6, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(683), "payment Fantastic Fresh Chips", "24/7", 3, 7, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(852), "Division withdrawal Avon", "Solutions", 3, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(2049), "Granite Metal Focused", new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(2076), "haptic", 7, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(2298), "Bedfordshire Rustic Plastic Tuna Consultant", null, "Generic", 3, 6, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(2424), "primary Generic Granite Pizza", null, "bluetooth", 7, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(2526), "engineer e-commerce", null, "software", 25, 9, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(2630), "Research", null, "Platinum", 25, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(2740), "Bedfordshire Generic Granite Shoes", "synthesize", 19, 6, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(2830), "Facilitator", "circuit", 10, 9, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(4022), "Gorgeous Representative", new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(4049), "grey", 4, 6, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "Description", "Name", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(4185), "Incredible Metal Chicken", "Tactics", 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(5487), "tertiary blue neural", "copy", 24, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(6107), "ROI Flats", null, "Cove", 27, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(6264), "Persevering Rufiyaa", null, "Tactics", 1, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(6433), "AI Intelligent Steel Salad", "cross-platform", 9, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(6547), "District District Phased", "Liaison", 27, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(6650), "payment ROI collaborative", "e-markets", 2, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(6756), "Gorgeous turquoise Baby", "Shore", 10, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(6879), "Ergonomic Cotton Tuna Handcrafted Granite Bacon", "deposit", 13, 6, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(7056), "user-centric Refined Plastic Chicken", "Cambridgeshire", 30, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(7155), "USB", null, "models", 11, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(7254), "systematic card", "Marketing", 9, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(7381), "Practical Plastic Shoes Practical knowledge user", new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(7392), "Handmade Metal Cheese", 8, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(7498), "input Square violet", null, "Representative", 9, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(7602), "ADP users Money Market Account", new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(7612), "Avon", 5, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(7711), "Tunnel payment Customer", new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(7722), "Lake", 12, 8, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(7868), "Village Grenada", new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(7881), "bifurcated", 5, 10, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(7974), "Turnpike", "FTP", 4, 10, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(8062), "Louisiana", null, "PCI", 11, 9, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(8161), "parallelism index Borders", null, "project", 6, 9, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(8248), "Concrete", "payment", 13, 6, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(8350), "Cambridgeshire Summit reinvent", new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(8361), "orange", 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(8445), "Vermont", new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(8456), "quantify", 4, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(8555), "Money Market Account index Steel", null, "Legacy", 2, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(8800), "Computers", null, "index", 28, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(9011), "markets Oregon", "Incredible Fresh Shoes", 26, 6, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(9225), "benchmark teal", "Mews", 9, 8, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 627, DateTimeKind.Local).AddTicks(9349), "Heights Estate Nauru", "Plastic", 7, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 555, DateTimeKind.Local).AddTicks(5516), "Dynamic Tactics Producer" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 561, DateTimeKind.Local).AddTicks(2981), "Principal Configuration Architect" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 561, DateTimeKind.Local).AddTicks(3153), "Legacy Metrics Designer" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 561, DateTimeKind.Local).AddTicks(3208), "Lead Metrics Architect" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 561, DateTimeKind.Local).AddTicks(3253), "Direct Creative Architect" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 561, DateTimeKind.Local).AddTicks(3301), "District Solutions Officer" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 561, DateTimeKind.Local).AddTicks(3460), "Central Identity Designer" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 561, DateTimeKind.Local).AddTicks(3520), "Senior Directives Administrator" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 561, DateTimeKind.Local).AddTicks(3566), "District Web Administrator" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 17, 4, 561, DateTimeKind.Local).AddTicks(3611), "Human Interactions Engineer" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2006, 2, 15, 22, 57, 15, 104, DateTimeKind.Unspecified).AddTicks(6679), "Deon21@yahoo.com", "Bell", "Russel", new DateTime(2021, 7, 3, 23, 17, 4, 586, DateTimeKind.Local).AddTicks(6301), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2004, 5, 22, 16, 47, 8, 487, DateTimeKind.Unspecified).AddTicks(2277), "Wiley_Von@hotmail.com", "Ottis", "Abshire", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(2795), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2003, 12, 22, 21, 25, 56, 29, DateTimeKind.Unspecified).AddTicks(934), "Willow_Dooley14@gmail.com", "Veronica", "Strosin", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(3333), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2004, 9, 26, 14, 20, 22, 159, DateTimeKind.Unspecified).AddTicks(8940), "Royal91@gmail.com", "Kari", "Fisher", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(3641), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2013, 1, 2, 9, 47, 4, 984, DateTimeKind.Unspecified).AddTicks(2108), "Damaris_Prosacco34@yahoo.com", "Rosalee", "Cronin", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(3862), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2003, 8, 2, 14, 35, 45, 804, DateTimeKind.Unspecified).AddTicks(95), "Ignacio.Beahan96@hotmail.com", "Deven", "Rodriguez", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(4250), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2003, 12, 30, 19, 27, 52, 736, DateTimeKind.Unspecified).AddTicks(5105), "Taurean.Streich72@yahoo.com", "Price", "Jerde", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(4461), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2010, 6, 8, 4, 31, 41, 943, DateTimeKind.Unspecified).AddTicks(2084), "Raymundo56@yahoo.com", "Mary", "Abshire", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(4703), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2012, 5, 25, 5, 34, 53, 656, DateTimeKind.Unspecified).AddTicks(8220), "Zakary_Blanda77@yahoo.com", "Diamond", "Leannon", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(4894), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2010, 4, 16, 20, 59, 55, 887, DateTimeKind.Unspecified).AddTicks(5248), "Patrick43@gmail.com", "Tod", "Bashirian", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(5076), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2005, 4, 27, 18, 11, 26, 660, DateTimeKind.Unspecified).AddTicks(3977), "Vernie38@yahoo.com", "Flavie", "Ward", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(5251), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2002, 9, 4, 20, 53, 56, 952, DateTimeKind.Unspecified).AddTicks(746), "Florencio_Howell@hotmail.com", "Una", "Bradtke", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(5486), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2004, 11, 27, 18, 9, 20, 611, DateTimeKind.Unspecified).AddTicks(1518), "Dock_Zieme@gmail.com", "Heather", "Kilback", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(5664), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2005, 10, 23, 22, 45, 27, 696, DateTimeKind.Unspecified).AddTicks(7564), "Omari.Vandervort@hotmail.com", "Gabrielle", "Emard", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(5844), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2012, 10, 20, 18, 56, 11, 51, DateTimeKind.Unspecified).AddTicks(5921), "Nina.Powlowski@gmail.com", "Norwood", "MacGyver", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(6018), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2002, 2, 23, 8, 9, 50, 879, DateTimeKind.Unspecified).AddTicks(2200), "Thaddeus_Waters@yahoo.com", "Cathryn", "Marquardt", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(6251), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2002, 7, 11, 12, 22, 40, 369, DateTimeKind.Unspecified).AddTicks(6688), "Ardith_Fritsch1@hotmail.com", "Jacky", "Borer", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(6449), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2002, 9, 9, 18, 52, 3, 429, DateTimeKind.Unspecified).AddTicks(5997), "Donna.Botsford49@gmail.com", "Chadrick", "Dicki", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(6623), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2014, 1, 5, 11, 32, 10, 704, DateTimeKind.Unspecified).AddTicks(6681), "Casper_Reichert@hotmail.com", "Nettie", "Howell", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(6858), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2011, 3, 26, 7, 2, 51, 816, DateTimeKind.Unspecified).AddTicks(3266), "Stanford.Hilpert@gmail.com", "Ed", "Fadel", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(7052), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2013, 7, 14, 18, 23, 26, 105, DateTimeKind.Unspecified).AddTicks(7816), "Mark_Mertz50@gmail.com", "Elvie", "Hagenes", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(7230), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2002, 11, 26, 22, 44, 18, 992, DateTimeKind.Unspecified).AddTicks(3212), "Ava.Okuneva@hotmail.com", "Kayley", "Cruickshank", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(7405), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2005, 5, 26, 10, 28, 58, 181, DateTimeKind.Unspecified).AddTicks(409), "Emilia_Rippin@hotmail.com", "Ocie", "Hermann", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(7579), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2011, 8, 30, 1, 1, 45, 789, DateTimeKind.Unspecified).AddTicks(7585), "Sidney.Bayer@yahoo.com", "Elaina", "Vandervort", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(7814), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2006, 10, 13, 5, 34, 39, 474, DateTimeKind.Unspecified).AddTicks(3602), "Joelle.Effertz@gmail.com", "Annalise", "Schiller", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(7993), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2014, 4, 2, 0, 6, 35, 815, DateTimeKind.Unspecified).AddTicks(2743), "Katarina.Waters@yahoo.com", "Isabell", "Cassin", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(8167), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2004, 10, 26, 23, 53, 57, 422, DateTimeKind.Unspecified).AddTicks(2843), "Brady_Smitham@yahoo.com", "Leann", "Zemlak", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(8393), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2005, 8, 13, 22, 24, 1, 208, DateTimeKind.Unspecified).AddTicks(3405), "Holly_Von37@gmail.com", "Maegan", "Orn", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(8576), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2003, 10, 2, 23, 52, 0, 153, DateTimeKind.Unspecified).AddTicks(5580), "Virginie31@gmail.com", "Devante", "Conroy", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(8949), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2011, 10, 2, 8, 49, 29, 640, DateTimeKind.Unspecified).AddTicks(3279), "Randal57@gmail.com", "Natalie", "Wuckert", new DateTime(2021, 7, 3, 23, 17, 4, 601, DateTimeKind.Local).AddTicks(9157), 3 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 19, new DateTime(2021, 7, 3, 23, 15, 4, 170, DateTimeKind.Local).AddTicks(3920), new DateTime(2021, 11, 26, 14, 27, 29, 791, DateTimeKind.Local).AddTicks(8681), "Ethiopian Birr circuit generating", "Central Infrastructure Strategist", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 19, new DateTime(2021, 7, 3, 23, 15, 4, 171, DateTimeKind.Local).AddTicks(5125), new DateTime(2022, 6, 8, 12, 56, 58, 816, DateTimeKind.Local).AddTicks(4258), "bandwidth-monitored", "Customer Optimization Administrator", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 2, new DateTime(2021, 7, 3, 23, 15, 4, 171, DateTimeKind.Local).AddTicks(5390), new DateTime(2022, 3, 28, 18, 19, 34, 131, DateTimeKind.Local).AddTicks(4709), "New Israeli Sheqel", "National Tactics Executive" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 12, new DateTime(2021, 7, 3, 23, 15, 4, 171, DateTimeKind.Local).AddTicks(6350), new DateTime(2022, 8, 21, 1, 26, 8, 430, DateTimeKind.Local).AddTicks(6954), "Optimization", "Principal Brand Facilitator", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 18, new DateTime(2021, 7, 3, 23, 15, 4, 171, DateTimeKind.Local).AddTicks(8403), new DateTime(2023, 3, 25, 17, 7, 14, 796, DateTimeKind.Local).AddTicks(8696), "New Leu Agent silver", "Legacy Functionality Consultant", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 29, new DateTime(2021, 7, 3, 23, 15, 4, 171, DateTimeKind.Local).AddTicks(9843), new DateTime(2022, 9, 11, 19, 55, 46, 248, DateTimeKind.Local).AddTicks(3087), "Auto Loan Account", "Senior Functionality Facilitator", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 28, new DateTime(2021, 7, 3, 23, 15, 4, 172, DateTimeKind.Local).AddTicks(54), new DateTime(2022, 4, 24, 22, 7, 19, 659, DateTimeKind.Local).AddTicks(1487), "ivory", "Forward Applications Designer", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 172, DateTimeKind.Local).AddTicks(2724), new DateTime(2023, 1, 18, 6, 44, 18, 493, DateTimeKind.Local).AddTicks(5579), "Creative payment Crescent", "Customer Program Producer", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 24, new DateTime(2021, 7, 3, 23, 15, 4, 172, DateTimeKind.Local).AddTicks(3378), new DateTime(2022, 12, 6, 7, 50, 39, 155, DateTimeKind.Local).AddTicks(2360), "Gardens", "Global Applications Coordinator" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 19, new DateTime(2021, 7, 3, 23, 15, 4, 172, DateTimeKind.Local).AddTicks(4738), new DateTime(2021, 12, 1, 10, 17, 18, 804, DateTimeKind.Local).AddTicks(6893), "capability Berkshire", "Investor Data Consultant", 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 181, DateTimeKind.Local).AddTicks(3304), "algorithm", new DateTime(2021, 7, 3, 23, 15, 4, 181, DateTimeKind.Local).AddTicks(7830), "Incredible Texas", 11, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 184, DateTimeKind.Local).AddTicks(3575), "Refined Wooden Mouse Fresh Grocery, Automotive & Automotive", null, "Squares redundant", 15, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 184, DateTimeKind.Local).AddTicks(6367), "Global", new DateTime(2021, 7, 3, 23, 15, 4, 184, DateTimeKind.Local).AddTicks(6399), "Tasty Frozen Ball Faroe Islands", 8, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 184, DateTimeKind.Local).AddTicks(6608), "Supervisor", "Fresh Curve", 19, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 184, DateTimeKind.Local).AddTicks(8097), "Colorado", null, "access unleash", 25, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 184, DateTimeKind.Local).AddTicks(8322), "Fiji Licensed Wooden Gloves", "Bangladesh cross-platform", 27, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 184, DateTimeKind.Local).AddTicks(9847), "Applications maroon New York", "panel encoding", 23, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(34), "Row", "olive generating", 3, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(220), "Response bypassing", "Investment Account Uzbekistan Sum", 28, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(2827), "open-source Reverse-engineered", new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(2856), "Self-enabling Administrator", 16, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(3045), "Avon", new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(3058), "Research orchestration", 8, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(3222), "Glens optical", "Kuwaiti Dinar motivating", 10, 8, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(4636), "technologies", "synergies Personal Loan Account", 30, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(4834), "Ohio Hollow", "hacking District", 30, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(5025), "Consultant solution-oriented payment", null, "Granite Handcrafted Metal Bike", 8, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(5268), "Rial Omani Handmade gold", new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(5284), "deliverables Ergonomic", 8, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(6687), "models cyan sticky", new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(6714), "Incredible Granite Table SDD", 8, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(6872), "Curve", new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(6885), "array Berkshire", 14, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(7030), "Synergistic Croatia", new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(7042), "Program Optimization", 9, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(7190), "Fantastic Rubber Shoes", "blockchains Western Sahara", 15, 8, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(7315), "Divide", "Berkshire Profound", 14, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(7618), "New Israeli Sheqel backing up", null, "Handcrafted Cotton Mouse bluetooth", 16, 10, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "Description", "Name", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(7758), "Cambridgeshire", "Implementation Bhutan", 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(9261), "Principal", "Books, Electronics & Grocery navigate", 16, 9, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(9491), "CFA Franc BCEAO Burgs Oklahoma", new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(9507), "AGP Bermudian Dollar (customarily known as Bermuda Dollar)", 28, 9, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(9697), "Refined Metal Ball Persistent Buckinghamshire", new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(9710), "Handcrafted Fresh Shoes fresh-thinking", 21, 10, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 185, DateTimeKind.Local).AddTicks(9979), "platforms pink", "Metal invoice", 12, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(120), "deposit", "infrastructure Texas", 12, 6, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(267), "California Rest Mayotte", "Oklahoma integrate", 7, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(423), "Ergonomic Frozen Keyboard Berkshire", "composite Granite", 23, 7, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(547), "panel", "blue Mills", 15, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(670), "Buckinghamshire", "New Hampshire Unbranded", 11, 9, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(791), "Bedfordshire", new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(803), "Circles New Jersey", 28, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(996), "Centers", "Argentine Peso National", 26, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(1172), "mobile Handcrafted Metal Tuna Fresh", null, "Incredible calculate", 2, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(1310), "Maryland South Carolina", new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(1323), "Texas systems", 23, 7, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(1460), "Home superstructure", new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(1472), "Credit Card Account users", 16, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(1632), "purple Intelligent Metal Ball", null, "Unbranded Fresh Ball Crest", 28, 6, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(1757), "Washington", null, "fuchsia transform", 23, 9, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(1967), "Ergonomic Wooden Ball Investment Account", "HTTP SAS", 12, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(2097), "Ergonomic", new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(2109), "Awesome Northern Mariana Islands", 12, 8, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(2230), "Personal Loan Account", new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(2244), "Steel sticky", 25, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(2388), "Awesome Wooden Mouse ADP", "intangible flexibility", 30, 7, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(2614), "Papua New Guinea", new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(2630), "Books & Computers Licensed Metal Gloves", 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(2854), "platforms Illinois Gorgeous Wooden Hat", null, "bluetooth invoice", 26, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(2990), "Home Loan Account", new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(3003), "Directives Isle", 18, 7, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(3143), "open-source Forint", new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(3157), "Frozen Secured", 18, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(3295), "Product", "Intelligent Fresh Shoes metrics", 14, 9, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(3433), "Future", "Montana Practical Metal Mouse", 6, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 186, DateTimeKind.Local).AddTicks(3581), "multi-byte Investor enable", "web-enabled Jersey", 22, 6, 0 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 52, DateTimeKind.Local).AddTicks(6317), "Chief Identity Associate" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 58, DateTimeKind.Local).AddTicks(3768), "National Accountability Orchestrator" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 58, DateTimeKind.Local).AddTicks(3919), "Direct Program Liaison" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 58, DateTimeKind.Local).AddTicks(3983), "Principal Branding Architect" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 58, DateTimeKind.Local).AddTicks(4039), "Human Intranet Agent" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 58, DateTimeKind.Local).AddTicks(4119), "Customer Web Producer" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 58, DateTimeKind.Local).AddTicks(4174), "Customer Interactions Officer" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 58, DateTimeKind.Local).AddTicks(4226), "Central Program Facilitator" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 58, DateTimeKind.Local).AddTicks(4278), "Regional Security Developer" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 23, 15, 4, 58, DateTimeKind.Local).AddTicks(4326), "Customer Branding Agent" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2012, 10, 3, 1, 51, 54, 710, DateTimeKind.Unspecified).AddTicks(8765), "Gwendolyn.Herzog19@yahoo.com", "Sabryna", "Conn", new DateTime(2021, 7, 3, 23, 15, 4, 84, DateTimeKind.Local).AddTicks(1865), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2007, 8, 4, 13, 42, 4, 267, DateTimeKind.Unspecified).AddTicks(1466), "Una62@gmail.com", "Merritt", "Zieme", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(2366), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2006, 5, 4, 3, 53, 29, 20, DateTimeKind.Unspecified).AddTicks(9430), "Vance.Padberg@yahoo.com", "Gene", "Price", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(2965), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2008, 9, 1, 0, 0, 35, 279, DateTimeKind.Unspecified).AddTicks(7524), "Johanna11@hotmail.com", "Vincenzo", "Sauer", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(3322), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2004, 5, 11, 17, 54, 37, 465, DateTimeKind.Unspecified).AddTicks(6700), "Dean_Kirlin19@gmail.com", "Jazmin", "Fay", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(3645), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2004, 7, 9, 17, 53, 1, 704, DateTimeKind.Unspecified).AddTicks(4916), "Jaylan.Gorczany@gmail.com", "Avery", "O'Hara", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(3883), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2002, 8, 3, 12, 39, 52, 686, DateTimeKind.Unspecified).AddTicks(2184), "Stuart23@hotmail.com", "Christophe", "Schmidt", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(4097), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2004, 8, 12, 16, 1, 54, 518, DateTimeKind.Unspecified).AddTicks(405), "Rhett14@hotmail.com", "Colton", "Murray", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(4415), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2005, 1, 28, 15, 47, 1, 120, DateTimeKind.Unspecified).AddTicks(2305), "Alanis.Kreiger@yahoo.com", "Brielle", "Friesen", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(4723), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2006, 4, 4, 7, 43, 36, 977, DateTimeKind.Unspecified).AddTicks(8387), "Brando74@yahoo.com", "Lydia", "Gusikowski", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(4944), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2009, 2, 23, 1, 20, 59, 20, DateTimeKind.Unspecified).AddTicks(6794), "Brice.Erdman99@yahoo.com", "Maximillian", "Hane", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(5155), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2014, 4, 16, 0, 4, 36, 303, DateTimeKind.Unspecified).AddTicks(1484), "Evans15@gmail.com", "Domenico", "Wiza", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(5363), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2011, 9, 9, 4, 45, 48, 432, DateTimeKind.Unspecified).AddTicks(3612), "Shayna_Fahey@yahoo.com", "Carter", "Hand", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(5631), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2007, 12, 20, 2, 41, 32, 526, DateTimeKind.Unspecified).AddTicks(1081), "Aron.Heller87@hotmail.com", "Ardith", "MacGyver", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(5841), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2009, 6, 29, 20, 31, 55, 891, DateTimeKind.Unspecified).AddTicks(440), "Jaylan_Toy@hotmail.com", "Alexandra", "Orn", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(6042), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2006, 8, 9, 7, 45, 15, 561, DateTimeKind.Unspecified).AddTicks(6935), "Tania.Cassin88@hotmail.com", "Vernon", "Macejkovic", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(6245), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2004, 5, 29, 22, 0, 6, 660, DateTimeKind.Unspecified).AddTicks(4581), "Bruce_Cremin90@yahoo.com", "Haven", "McClure", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(6562), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2012, 12, 1, 12, 46, 34, 565, DateTimeKind.Unspecified).AddTicks(8734), "Ressie.Crona36@gmail.com", "Furman", "Tromp", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(6795), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2010, 6, 14, 23, 44, 45, 294, DateTimeKind.Unspecified).AddTicks(8020), "Chance.Schaefer@hotmail.com", "Jewel", "Gerhold", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(7008), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2007, 2, 21, 3, 55, 8, 934, DateTimeKind.Unspecified).AddTicks(6638), "Ashleigh42@hotmail.com", "Carolina", "Dibbert", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(7226), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2006, 9, 6, 7, 25, 11, 278, DateTimeKind.Unspecified).AddTicks(3840), "Jarrell.Sawayn5@hotmail.com", "Luigi", "Maggio", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(7499), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2009, 10, 23, 7, 30, 26, 407, DateTimeKind.Unspecified).AddTicks(4332), "Pierre_Baumbach15@hotmail.com", "Nico", "Ebert", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(7735), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2006, 12, 26, 18, 2, 44, 653, DateTimeKind.Unspecified).AddTicks(6534), "Eugene43@hotmail.com", "Filomena", "Nitzsche", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(7940), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2002, 5, 4, 6, 51, 0, 642, DateTimeKind.Unspecified).AddTicks(706), "Josiah.Murphy55@hotmail.com", "Nora", "Sauer", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(8198), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2007, 1, 16, 6, 5, 56, 384, DateTimeKind.Unspecified).AddTicks(5185), "Camryn.Effertz39@hotmail.com", "Alejandra", "Collins", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(8421), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2013, 5, 26, 14, 57, 39, 787, DateTimeKind.Unspecified).AddTicks(9914), "Reggie0@hotmail.com", "Cielo", "Kuphal", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(8624), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2002, 4, 1, 10, 13, 20, 891, DateTimeKind.Unspecified).AddTicks(4553), "Madeline.Frami@yahoo.com", "Celestino", "Rosenbaum", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(8827), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2010, 2, 18, 12, 32, 25, 771, DateTimeKind.Unspecified).AddTicks(804), "Fausto_Veum32@hotmail.com", "Brooke", "Blick", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(9081), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2005, 6, 12, 16, 38, 12, 259, DateTimeKind.Unspecified).AddTicks(9207), "Lonny_Beatty@hotmail.com", "Jaren", "Kihn", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(9293), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2008, 8, 26, 5, 19, 31, 317, DateTimeKind.Unspecified).AddTicks(1854), "Millie_Jacobs71@yahoo.com", "Taya", "McDermott", new DateTime(2021, 7, 3, 23, 15, 4, 159, DateTimeKind.Local).AddTicks(9497), 5 });
        }
    }
}
