using System;
using System.Collections.Generic;

#nullable enable

namespace Homework.DAL.Entities
{
    public class Project
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }

        public User Author { get; set; }
        public Team Team { get; set; }
        public ICollection<Task> Tasks { get; set; }
    }
}